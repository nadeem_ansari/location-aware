//
//  ViewController.swift
//  Location Aware
//
//  Created by Nadeem Ansari on 5/9/17.
//  Copyright © 2017 Nadeem Ansari. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var latitudeLBL: UILabel!
    
    @IBOutlet weak var longitudeLBL: UILabel!
    
    @IBOutlet weak var courseLBL: UILabel!
    
    @IBOutlet weak var speedLBL: UILabel!
    
    @IBOutlet weak var altitudeLBL: UILabel!
    
    @IBOutlet weak var addressLBL: UILabel!
    
    var locationManager:CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations[0]
        
        let latitude = userLocation.coordinate.latitude
        
        let longitude = userLocation.coordinate.longitude
        
        latitudeLBL.text = String(latitude)
        
        longitudeLBL.text = String(longitude)
        
        courseLBL.text = userLocation.course.description
        
        speedLBL.text = userLocation.speed.description + " mph"
        
        altitudeLBL.text = userLocation.altitude.description
        
        CLGeocoder().reverseGeocodeLocation(userLocation) { (placemarks, error) in
            
            if error != nil {
                print(error!)
            }
            else{
                if let placemark = placemarks?[0] {
                    var subThoroughfare = ""
                    
                    if placemark.subThoroughfare != nil{
                        subThoroughfare = placemark.subThoroughfare!
                    }
                    
                    var thoroughfare = ""
                    
                    if placemark.thoroughfare != nil {
                        thoroughfare = placemark.thoroughfare!
                    }
                    
                    var sublocality = ""
                    
                    if placemark.subLocality != nil {
                        sublocality = placemark.subLocality!
                    }
                    
                    var subAdministrativeArea = ""
                    
                    if placemark.subAdministrativeArea != nil {
                        subAdministrativeArea = placemark.subAdministrativeArea!
                    }
                    
                    var postalCode = ""
                    
                    if placemark.postalCode != nil {
                        postalCode = placemark.postalCode!
                    }
                    
                    var country = ""
                    
                    if placemark.country != nil {
                        country = placemark.country!
                    }
                    
                    self.addressLBL.text = subThoroughfare+thoroughfare+"\n"+sublocality+subAdministrativeArea+"\n"+postalCode+"\n"+country
                }
                
            }
        }
        
    }
}

